package com.citi.training.stocks.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.stocks.model.Market;

public interface MarketDao extends CrudRepository<Market, Long> {}
