package com.citi.training.stocks.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.stocks.model.Stock;

public interface StockDao extends CrudRepository<Stock, Long> {}
