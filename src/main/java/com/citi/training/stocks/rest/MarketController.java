package com.citi.training.stocks.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.stocks.dao.MarketDao;
import com.citi.training.stocks.model.Market;

/**
 * This is the REST Interface to Manage Market objects.
 * 
 * @author Frank
 * 
 * @see Market
 *
 */
@RestController
@RequestMapping("/markets")
public class MarketController {

    private static final Logger LOG = LoggerFactory.getLogger(MarketController.class);

    @Autowired
    private MarketDao marketDao;

    @RequestMapping(method=RequestMethod.GET)
    public Iterable<Market> findAll() {
        LOG.info("HTTP GET findAll()");
        return marketDao.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Market findById(@PathVariable long id) {
        LOG.info("HTTP GET findById() id=[" + id + "]");
        return marketDao.findById(id).get();
    }

    @RequestMapping(method=RequestMethod.POST)
    public HttpEntity<Market> save(@RequestBody Market market) {
        LOG.info("HTTP POST save() market=[" + market + "]");
        return new ResponseEntity<Market>(marketDao.save(market),
                                         HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value=HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id) {
        LOG.info("HTTP DELETE delete() id=[" + id + "]");
        marketDao.deleteById(id);
    }
}
