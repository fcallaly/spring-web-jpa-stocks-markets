package com.citi.training.stocks.rest;

import java.util.NoSuchElementException;

import javax.annotation.Priority;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Priority(1)
public class DefaultExceptionHandler {

    private static final Logger logger =
            LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler(value = {NoSuchElementException.class, EmptyResultDataAccessException.class})
    public ResponseEntity<Object> noSuchElementExceptionHandler(Exception ex) {
        logger.warn(ex.toString());
        return new ResponseEntity<>("{\"message\": \"The requested entity was not found.\"}",
                                    HttpStatus.NOT_FOUND);
    }
}